var Component = require('./component');

class TextBox extends Component {
  pre() {
    return `<Col style={{ padding: 10 }}><Item><Input placeholder="${this.placeholder}"/>`;
  }

  post() {
    return `</Item></Col>`;
  }
}

module.exports = TextBox;
