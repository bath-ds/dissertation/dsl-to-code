var Component = require('./component');
var generate_id = require('../generate-id');

class DropdownItem extends Component {
  pre() {
    const id = generate_id();
    return `<Picker.Item label="${this.label}" value="key_${id}" />`;
  }

  post() {
    return '';
  }
}

module.exports = DropdownItem;