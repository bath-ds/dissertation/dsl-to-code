var Component = require('./component');
var generate_id = require('../generate-id');

class CheckBox extends Component {
  pre() {
    const id = generate_id();
    return `<Col style={{ padding: 10 }}>
      <ListItem
      onPress={p =>
        this.setState({
          chkbox_${id}: !get(this, "state.chkbox_${id}", false)
        })
      }
      >
      <CheckBox checked={get(this, "state.chkbox_${id}", false)} />
      <Body>
    `;
  }

  post() {
    return `</Body></ListItem></Col>`;
  }
}

module.exports = CheckBox;