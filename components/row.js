var Component = require('./component');

class Row extends Component {
  pre() {
    return `<${this.tree.name}>`;
  }

  post() {
    return `</${this.tree.name}>`;
  }
}

module.exports = Row;
