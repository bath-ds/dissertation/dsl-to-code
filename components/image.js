var Component = require("./component");

class Image extends Component {
  pre() {
    let s = "<Col>";
    if (this.link_to) {
      s += `<TouchableOpacity
        onPress={() => this.props.navigation.navigate("page-${this.link_to}")}
        >`;
    }
    s += `<${this.tree.name} source={{ uri: "${this.source}" }} style={{ height: 200 }} />`;

    return s;
  }

  post() {
    let s = "";
    if (this.link_to) {
      s += "</TouchableOpacity>";
    }
    s += "</Col>";

    return s;
  }
}

module.exports = Image;
