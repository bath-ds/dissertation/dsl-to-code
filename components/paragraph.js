var Component = require("./component");

class Paragraph extends Component {
  pre() {
    let s = "<Col style={{ padding: 10 }}>";
    if (this.link_to) {
      s += `<TouchableOpacity
        onPress={() => this.props.navigation.navigate("page-${this.link_to}")}
        >`;
    }
    s += `<Text>${this.tree.block.nodes[0].val}`;

    return s;
  }

  post() {
    let s = "</Text>";
    if (this.link_to) {
      s += "</TouchableOpacity>";
    }
    s += "</Col>";

    return s;
  }
}

module.exports = Paragraph;
