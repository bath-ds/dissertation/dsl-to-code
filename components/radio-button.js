var Component = require('./component');
var generate_id = require('../generate-id');

class RadioButton extends Component {
  constructor(tree) {
    super(tree);
    this.id = generate_id();
  }

  pre() {
    return `<Col style={{ padding: 10 }}>
      <ListItem
        onPress={p =>
          this.setState({
            radio_${this.id}: !get(this, "state.radio_${this.id}", false)
          })
        }
      >
        <Left>
    `;
  }

  post() {
    return `
        </Left>
        <Right>
        <Radio selected={get(this, "state.radio_${this.id}", false)} />
    </Right></ListItem></Col>`;
  }
}

module.exports = RadioButton;