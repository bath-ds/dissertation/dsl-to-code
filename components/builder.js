const Component = require("./component");
const Container = require("./container");
const Button = require("./button");
const Row = require("./row");
const TitleBar = require("./title-bar");
const Text = require("./text");
const Image = require("./image");
const TextBox = require("./textbox");
const RadioButton = require("./radio-button");
const CheckBox = require("./checkbox");
const Dropdown = require("./dropdown");
const DropdownItem = require("./dropdown-item");
const Heading = require("./heading");
const Paragraph = require("./paragraph");

module.exports = (tree, params) => {
  switch (tree.name) {
    case "Container":
      return new Container(tree, params);
    case "Button":
      return new Button(tree);
    case "Row":
      return new Row(tree);
    case "TitleBar":
      return new TitleBar(tree);
    case "Text":
      return new Text(tree);
    case "Image":
      return new Image(tree);
    case "TextBox":
      return new TextBox(tree);
    case "RadioButton":
      return new RadioButton(tree);
    case "CheckBox":
      return new CheckBox(tree);
    case "Dropdown":
      return new Dropdown(tree);
    case "DropdownItem":
      return new DropdownItem(tree);
    case "Heading":
      return new Heading(tree);
    case "Paragraph":
      return new Paragraph(tree);
    default:
      return new Component(tree);
  }
};
