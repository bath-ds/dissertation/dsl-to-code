const Component = require("./component");

class TitleBar extends Component {
  pre() {
    return `
    <Header>
      <Left>
        <Button transparent onPress={() => this.props.navigation.goBack()}>
          <Icon name='arrow-back' />
        </Button>
      </Left>
      <Body>
        <Title>${this.title}</Title>
      </Body>
      <Right>
      </Right>
    </Header>`;
  }

  post() {
    return "";
  }
}

module.exports = TitleBar;
