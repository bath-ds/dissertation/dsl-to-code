class Component {
  constructor(tree) {
    this.tree = tree;
    for (let i = 0; i < tree.attrs.length; i++) {
      this[tree.attrs[i].name] = JSON.parse(tree.attrs[i].val);
    }
  }

  pre() {
    return `<Col><${this.tree.name}>`;
  }

  post() {
    return `</${this.tree.name}></Col>`;
  }
}

module.exports = Component;