var Component = require('./component');

class Button extends Component {
  pre() {
    if (this.link_to) {
      return `<Col style={{ padding: 10 }}><${this.tree.name} block onPress={() => this.props.navigation.navigate("page-${this.link_to}")}>`;
    }
    return `<Col style={{ padding: 10 }}><${this.tree.name} block>`;
  }

  post() {
    return `</${this.tree.name}></Col>`;
  }
}

module.exports = Button;
