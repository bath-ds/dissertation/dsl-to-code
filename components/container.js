var Component = require("./component");
var get = require("lodash.get");

class Container extends Component {
  constructor(tree, params) {
    super(tree);
    this.content = get(params, "content", false);
  }

  pre() {
    let s = `<${this.tree.name}>`;
    if (this.content) {
      s += `<Content><Grid>`;
    }
    return s;
  }

  post() {
    let s = "";
    if (this.content) {
      s += `</Grid></Content>`;
    }
    s += `</${this.tree.name}>`;
    return s;
  }
}

module.exports = Container;
