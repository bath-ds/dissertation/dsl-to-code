var Component = require('./component');
var generate_id = require('../generate-id');

class Dropdown extends Component {
  pre() {
    const id = generate_id();
    return `<Col style={{ padding: 10 }}>
      <Picker
      mode="dropdown"
      selectedValue={get(this, 'state.dropdown_${id}', false)}
      onValueChange={(value) => this.setState({ dropdown_${id}: value })}
      placeholder="${this.placeholder}"
      >
    `;
  }

  post() {
    return `</Picker></Col>`;
  }
}

module.exports = Dropdown;