const builder = require("./components/builder");
const get = require("lodash.get");

const compile = (tree, rowFlag = false, compParams = {}, result = "") => {
  if (tree.type == "Block") {
    // go straight to nodes
    let flag = false;
    for (let i = 0; i < tree.nodes.length; i++) {
      const prev = i == 0 ? false : tree.nodes[i - 1];
      if (prev && (prev.name == "TitleBar" || prev.name == "Container")) {
        result += "<Content><Grid>";
        flag = true;
      }
      if (
        !rowFlag &&
        tree.nodes[i].name != "TitleBar" &&
        tree.nodes[i].name != "Container" &&
        tree.nodes[i].name != "Row" &&
        tree.nodes[i].name != "DropdownItem" &&
        tree.nodes[i].type != "Text"
      ) {
        result += "<Row>";
        result += compile(tree.nodes[i]);
        result += "</Row>";
      } else {
        let params = null;
        if (
          i == 0 &&
          get(tree, "nodes[0].block.nodes[0].name", "TitleBar") != "TitleBar"
        ) {
          params = { content: true };
        }
        result += compile(tree.nodes[i], false, params);
      }
    }
    if (flag) {
      result += "</Grid></Content>";
    }
  } else if (tree.type == "Tag") {
    const component = builder(tree, compParams);

    result += component.pre();
    if (tree.hasOwnProperty("block") && tree.name != "Text" && tree.name != "Heading" && tree.name != "Paragraph") {
      result += compile(tree.block, rowFlag || tree.name == "Row");
    }
    result += component.post();
  } else {
    // for type == 'Text'
    result += `<Text>${tree.val}</Text>`;
  }
  return result;
};

module.exports = compile;
