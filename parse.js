const lex = require('pug-lexer');
const parse = require('pug-parser');
const filters = require('pug-filters');
const stripComments = require('pug-strip-comments');


module.exports = pugString => {
  return filters.handleFilters(
    parse(
      stripComments(lex(pugString), { stripUnbuffered: true, stripBuffered: true }),
    ),
  );
};