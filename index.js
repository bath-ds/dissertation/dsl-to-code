const fs = require("fs-extra");
const path = require("path");
const prettier = require("prettier");
const glob = require("glob");
const parsePug = require("./parse");
const compile = require("./compile");

const args = require("./args.js");

const main = ({ input, output }) => {
  let inputFilenames = glob.sync("page_@(a|b|c|d|0|1|2|3|4).pug", {
    cwd: input
  });

  // sort alphabets first, then digits
  const f_alpha = inputFilenames.filter(i => /^page_\D\.pug$/.test(i));
  const f_num = inputFilenames.filter(i => /\d/.test(i));
  inputFilenames = f_alpha.concat(f_num);

  inputFilenames.forEach(inputFilename => {
    // read input file
    const inputFull = path.join(input, inputFilename);
    const pugString = fs.readFileSync(inputFull, "utf8");

    // parse and compile page
    const tree = parsePug(pugString);
    const jsx = compile(tree);

    // output jsx file
    const pageTemplate = fs.readFileSync(
      path.join(__dirname, "templates", "page.js"),
      "utf8"
    );
    let outputFilename = inputFilename.replace(/_/g, "-").replace(/pug/g, "js");
    outputFilename = path.join(output, outputFilename);
    const pageString = prettier.format(
      pageTemplate.replace(/\<page\/\>/g, jsx),
      {
        parser: "babel"
      }
    );
    fs.writeFileSync(outputFilename, pageString, "utf8");
  });

  // create main app file
  if (inputFilenames.length > 0) {
    const imps = inputFilenames.map(
      i =>
        `import Page${i.charAt(5).toUpperCase()} from './page-${i.charAt(5)}';`
    );
    const pages = inputFilenames.map(
      i => `"page-${i.charAt(5)}": { screen: Page${i.charAt(5).toUpperCase()} }`
    );
    const init = `page-${inputFilenames[0].charAt(5)}`;

    // output app file
    const pageTemplate = fs.readFileSync(
      path.join(__dirname, "templates", "App.js"),
      "utf8"
    );
    const outputFilename = path.join(output, "App.js");
    let pageString = pageTemplate
      .replace(/\<imports\/\>/g, imps.join(" "))
      .replace(/\<pages\/\>/g, pages.join(","))
      .replace(/\<init\/\>/g, init);
    pageString = prettier.format(pageString, {
      parser: "babel"
    });
    fs.writeFileSync(outputFilename, pageString, "utf8");
  }
};

main(args);
