const fs = require("fs-extra");
const ArgumentParser = require("argparse").ArgumentParser;
const parser = new ArgumentParser();

const dirChecker = arg => {
    try {
      if (!fs.lstatSync(arg).isDirectory()) {
        parser.error(`${arg} is not a directory.`);
      }
    } catch (err) {
      parser.error(`${arg} directory does not exist.`);
    }
    return arg;
};

parser.addArgument("input", {
  help: "Full path to the input directory.",
  type: dirChecker
});
parser.addArgument("output", {
  help: "Full path to the output directory.",
  type: dirChecker
});

module.exports = parser.parseArgs();