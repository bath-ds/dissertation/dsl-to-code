import React from "react";
import { createStackNavigator, createAppContainer } from "react-navigation";
<imports/>

const AppNavigator = createAppContainer(
  createStackNavigator(
    {
      <pages/>
    },
    {
      initialRouteName: "<init/>",
      defaultNavigationOptions: {
        header: null
      }
    }
  )
);

export default (App = () => {
  return <AppNavigator />;
});
