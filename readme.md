# dsl-to-code

Transforms input [Pug](https://pugjs.org) files from [sketch-to-dsl](https://gitlab.com/bath-ds/dissertation/sketch-to-dsl) into [React Native](https://facebook.github.io/react-native/) code.

See the main [Lines of Code repository](https://gitlab.com/bath-ds/dissertation/lines-of-code).

## Prerequisites

- [Node.js](https://nodejs.org/en/)

## Installation

Clone the repository and install all required packages by executing `npm install`.

## Running

Run the `index.js` script with the following parameters.

    node index.js <input_directory> <output_directory>

    input_directory:
      Full path to the input directory containing the Pug files.

    output_directory:
      Full path to the output directory where the source code will be generated. Typically, this would be the root directory of a React Native project.
